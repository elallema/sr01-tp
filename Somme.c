#include <stdlib.h>
#include <stdio.h>
#include<unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include"ex3.h"

int main (int argc, char *argv[])
{

if(argc<4||argc>4){//on vérifie qu'il y a le bon nombre d'arguments
    printf("Erreur dans le nb d'arguments %d \n", argc);
    return EXIT_FAILURE;

}
int i,j;
FILE * f1;
FILE * f2;
int N=atoi(argv[3]);//on convertit la chaine en int
printf("N Somme=%d\n", N);

int **mat1= malloc(N * sizeof(int *));//allocation mémoire de la matrice 1
	if(mat1 == NULL)
		{
		fprintf(stderr, "out of memory mat1\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		mat1[i] = malloc(N * sizeof(int));
		if(mat1[i] == NULL)
			{
			fprintf(stderr, "out of memory mat1\n");
		return EXIT_FAILURE;
			}
		}


int **mat2= malloc(N * sizeof(int *));//allocation mémoire de la matrice 2
	if(mat2 == NULL)
		{
		fprintf(stderr, "out of memory mat2\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		mat2[i] = malloc(N * sizeof(int));
		if(mat2[i] == NULL)
			{
			fprintf(stderr, "out of memory mat2\n");
		return EXIT_FAILURE;
			}
		}


int **sum= malloc(N * sizeof(int *));//allocation de la matrice résultat de la somme
	if(sum == NULL)
		{
		fprintf(stderr, "out of memory sum\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		sum[i] = malloc(N * sizeof(int));
		if(sum[i] == NULL)
			{
			fprintf(stderr, "out of memory sum\n");
		return EXIT_FAILURE;
			}
		}


pid_t *pid=malloc(N*sizeof(int));//allocation mémoire du tableau de PID où seront stockés les N fork
int *fd=malloc(2*N*sizeof(int));//allocation du tableaux des N descripteurs de taille 2 pour les N pipe (un tube par processus fils, chaque fils communique avec le père)


for (int i = 0; i < N; i++) {//on crée les N pipe
        pipe(&fd[2*i]);


        if(pipe(&fd[2*i]) != 0)

        {

            fprintf(stderr, "Erreur de création du tube.\n");
            return EXIT_FAILURE;

        }
    }
/*
int fd[2];

if (pipe(fd) < 0) {
     perror("pipe");
     exit(EXIT_FAILURE);
   }
*/

f1=fopen(argv[1], "rb");//lecture de la matrice 1 dans le fichier fichier1.bin

if (f1 != NULL){

        // On peut lire et écrire dans le fichier
        for(i=0;i<N;i++){
            fread(mat1[i] , sizeof(mat1[i]), N , f1);
        }

        afficher_mat(N, mat1);//on affiche la matrice lue mat1
        fclose(f1);

    }else{


        printf("Impossible d'ouvrir le fichier %s\n", argv[1]);
        return EXIT_FAILURE;

    }
printf("lecture f1\n");

f2=fopen(argv[2], "rb");//lecture de la matrice 2 dans le fichier2.bin
printf("avant lecture f2\n");
if (f2 != NULL){

        // On peut lire et écrire dans le fichier
        for(i=0;i<N;i++){
            fread(mat2[i] , sizeof(mat2[i]), N , f2);
            printf("lecture b!!\n");
        }
        afficher_mat(N, mat2);//on affiche la matrice lue mat2


        fclose(f2);

    }

    else{


        printf("Impossible d'ouvrir le fichier %s\n", argv[2]);
        return EXIT_FAILURE;

    }
printf("lecture f2!!\n");

for(i=0;i<N;i++){
    pid[i]=fork();//on crée les N processus

    if(pid[i]==0){


        close(fd[2*i]) ;//on ferme la sortie du tube (lecture)

        int *tab=malloc(N*sizeof(int));

        add(N, mat1, mat2, tab, &i);//on additionne les lignes i des deux matrices

        for(j=0;j<N;j++){
            printf("tab[%d]=%d\n", j, tab[j]);//on affiche le résultat de la ligne
        }
        write(fd[2*i+1], tab, N*sizeof(int));//on écrit ce résultat dans le tube du processus pid[i]

        close(fd[2*i+1]);//on ferme l'entrée du tube (écriture)

        exit(EXIT_SUCCESS);

    }
    else if(pid[i]==-1){
        perror("Erreur fork\n");
        exit(EXIT_FAILURE);
    }
}
close(fd[2*i+1]); // close the unused write end of the pipe
// parent
int exit_status = EXIT_SUCCESS;
   { // read partial sums


        for(i=0;i<N;i++){
            read(fd[2*i], sum[i], N*sizeof(int));//le père lit les donnée écrites par le fils dans le tube et les stocke à la ligne i de la matrice résultat sum
        }
        close(fd[2*i]);// on ferme la sortie du tube

        int child;
        for (child=0; child < N; ) {
        int status = 0;
        if (waitpid(pid[child], &status, 0) == -1) {
         if (errno != EINTR) {
           perror("waitpid");
           exit_status = EXIT_FAILURE;
         }
         else // try again with the same child
           continue;
       }
       else if (WIFEXITED(status)) {//WIFEXITED non nul si le fils s'est terminé normalement
         int returncode = WEXITSTATUS(status);//WEXITSTATUS donne le code de retour de l'appel exit()
         if (returncode != 0)
           fprintf(stderr, PROCNAME ": %d child returned non-zero status %d\n",
                   pid[child], returncode);
       }
       else if (WIFSIGNALED(status)) {//si le fils s'est terminé à cause d'un signal non intercepté
         fprintf(stderr, PROCNAME ": %d child killed by signal %d\n",
                 pid[child], WTERMSIG(status));
       }
       else {
         fprintf(stderr, PROCNAME ": %d child returned unexpected status %d\n",
                 pid[child], status);
       }
       ++child; // next child
     }


printf("%s Résultat de la somme %s\n", B, B);
afficher_mat(N, sum);//affichage de la matrice résultat

//on libère l'espace mémoire alloué dynamiquement à la fin du programme :
for (i = 0; i < N; ++i){
        free(mat1[i]);
        free(mat2[i]);
        free(sum[i]);
}
    free(mat1);
    free(mat2);
    free(sum);
    free(pid);
    free(fd);

   }
   exit(exit_status);

}
void afficher_mat(int N, int **mat){
    int i,j;
    for(i = 0; i<N; i++){
        for(j = 0; j<N; j++){
            printf("%d  ",mat[i][j]);

        }
        printf("\n");
    }


}

void add(int N, int **m1, int **m2, int *t, int *i){

    int k;
    for(k=0;k<N;k++){
        t[k]=m1[*i][k]+m2[*i][k];
    }

}
