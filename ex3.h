#ifndef EX3_H_INCLUDED
#define EX3_H_INCLUDED
#define MAX 10
#define B "**************"
#define PROCNAME "parallel-sum-fork"


void add(int N, int **m1, int **m2, int *t, int *i);
void prod(int N, int **m1, int **m2, int *mult, int *i);
void afficher_mat(int N, int **mat);
void saisir_mat(int N, int **mat);
void gen_mat(int N, int **mat);

typedef struct{
   int decripteur[2];
}t_pipe;

#endif // EX3_H_INCLUDED
