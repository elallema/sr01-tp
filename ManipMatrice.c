#include <stdlib.h>
#include <stdio.h>
#include<unistd.h>
#include <sys/types.h>
#include <errno.h>
#include <sys/wait.h>
#include<time.h>
#include"ex3.h"

int main(){

int i,j;
int N;
int choix;

do{
    system("clear");
    printf("%s Initialisation %s\n", B, B);
    printf("Entrez la taille de vos matrices carrées (nb de lignes)\n");
    scanf("%d",&N);
}while(N<=0||N>MAX);

int **matrice1= malloc(N * sizeof(int *));//allocation mémoire de la première matrice

	if(matrice1 == NULL)
		{
		fprintf(stderr, "out of memory matrice1\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		matrice1[i] = malloc(N * sizeof(int));
		if(matrice1[i] == NULL)
			{
			fprintf(stderr, "out of memory matrice1\n");
		return EXIT_FAILURE;
			}
		}
int **matrice2= malloc(N * sizeof(int *));//allocation mémoire de la deuxième matrice
	if(matrice2 == NULL)
		{
		fprintf(stderr, "out of memory matrice2\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		matrice2[i] = malloc(N * sizeof(int));
		if(matrice2[i] == NULL)
			{
			fprintf(stderr, "out of memory matrice2\n");
		return EXIT_FAILURE;
			}
		}
int **matrice= malloc(N * sizeof(int *));
	if(matrice == NULL)
		{
		fprintf(stderr, "out of memory matrice\n");
		return EXIT_FAILURE;
		}
	for(i = 0; i < N; i++)
		{
		matrice[i] = malloc(N * sizeof(int));
		if(matrice[i] == NULL)
			{
			fprintf(stderr, "out of memory matrice\n");
		return EXIT_FAILURE;
			}
		}

do{
    printf("%s Menu %s\n", B, B);
    printf("1 : Saisir les 2 matrices.\n");
    printf("2 : Générer aléatoirement les matrices (chiffres entre 0 et 9)\n");
    scanf("%d",&choix);
}while(choix!=1 &&choix!=2);

    if(choix==1){
        printf("Saisir la matrice 1 :");
        saisir_mat(N, matrice1);
        printf("Affichage de la matrice 1 : \n");
        afficher_mat(N, matrice1);
        printf("Saisir la matrice 2 :");
        saisir_mat(N, matrice2);
        printf("Affichage de la matrice 2 : \n");
        afficher_mat(N, matrice2);

    }else{

        gen_mat(N, matrice1);//appel de la fonction qui génère aléatoirement une matrice de taille N (chiffre de 0 à 9)
        printf("Affichage de la matrice 1 : \n");
        afficher_mat(N, matrice1);
        sleep(1);//on demande au processus d'attendre afin que les deux matrices n'aient pas les même valeurs (la fonction time est liée au temps écoulé)
        gen_mat(N, matrice2);
        printf("Affichage de la matrice 2 : \n");
        afficher_mat(N, matrice2);


    }


    FILE * f1;
    FILE *f2;
    int k,l;

    f1=fopen("fichier1.bin", "wb");//créé et ouvert en mode lecture binaire
    if (f1 != NULL)

    {

        // On peut lire et écrire dans le fichier

        for(i=0;i<N;i++){
            fwrite(matrice1[i] , sizeof(matrice1[i]), N , f1);//on écrit dans le fichier binaire fichier1.bin
        }
        fclose(f1);//fermeture

//Test de lecture (cette étape fait office de test pour savoir si les données ont bien été écrite :
        f1=fopen("fichier1.bin", "rb");//ouverture en mode lecture binaire
        for(i=0;i<N;i++){
            fread(matrice[i] , sizeof(matrice[i]), N , f1);//on lit le fichier et on stocke la matrice lue dans matrice
        }
           for(k=0;k<N;k++){
            for(l=0;l<N;l++){
                printf("matrice1_lue[%d][%d]=%d\n",k,l,matrice[k][l]);
            }
            }

        fclose(f1);

    }

    else

    {


        printf("Impossible d'ouvrir le fichier fichier1.bin\n");
        return EXIT_FAILURE;

    }
//On effectue les étapes précédentes pour écrire la matrice 2 dans le fichier2.bin :
    f2=fopen("fichier2.bin", "wb");
    if (f2 != NULL)

    {

        // On peut lire et écrire dans le fichier
       for(i=0;i<N;i++){
            fwrite(matrice2[i] , sizeof(matrice2[i]), N , f2);
        }
        fclose(f2);


        f2=fopen("fichier2.bin", "rb");
        for(i=0;i<N;i++){
            fread(matrice[i] , sizeof(matrice[i]), N , f2);
        }

        for(k=0;k<N;k++){
            for(l=0;l<N;l++){
                printf("matrice2_lue[%d][%d]=%d\n",k,l,matrice[k][l]);
            }
        }

    fclose(f2);

    choix=0;
    char machaine[100];
    sprintf(machaine,"%d",N);//conversion de l'entier N en char

    do{
    printf("%s Menu %s\n", B, B);
    printf("Que souhaitez-vous faire ? \n");
    printf("1 : Somme des matrices\n");
    printf("2 : Produit des matrices\n");
    scanf("%d",&choix);

    if(choix==1){

        char *arguments1[] = { "Somme", "fichier1.bin", "fichier2.bin", machaine, NULL };
        if (execv("./Somme", arguments1) == -1) { //appel de la fonction execv pour exécuter le programme Somme
            perror("execv");

            return EXIT_FAILURE;

    }

    }if(choix==2){

   	 char *arguments2[] = { "Produit", "fichier1.bin", "fichier2.bin", machaine, NULL };
         if (execv("./Produit", arguments2) == -1) {//appel de la fonction execv pour exécuter le programme Produit
            perror("execv");
            return EXIT_FAILURE;

    }
    }
}while(choix<1||choix>2);
    //on libère l'espace mémoire alloué dynamiquement à la fin du programme
    for (i = 0; i < N; ++i){
        free(matrice1[i]);
        free(matrice2[i]);
        free(matrice[i]);
}
    free(matrice1);
    free(matrice2);
    free(matrice);


return EXIT_SUCCESS;
}



    else

    {


        printf("Impossible d'ouvrir le fichier fichier2.bin\n");
        return EXIT_FAILURE;

    }
}


void saisir_mat(int N, int **mat){
int k,l;

for(k=0;k<N;k++){
        for(l=0;l<N;l++){
            printf("matrice[%d][%d]=?\n",k+1,l+1);
            scanf("%d",&mat[k][l]);
            }
        }



}
void gen_mat(int N, int **mat){
int k,l;
         srand(time(NULL));
        for(k= 0; k<N; k++){
            for(l = 0;l<N; l++){
            mat[k][l] = rand()% 10;
            }
    }



}
void afficher_mat(int N, int **mat){
    int i,j;
    for(i = 0; i<N; i++){
        for(j = 0; j<N; j++){
            printf("%d  ",mat[i][j]);

        }
        printf("\n");
    }


}
